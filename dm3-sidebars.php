<?php
/**
 * Plugin Name: Dm3Sidebars
 * Author: incrediblebytes
 * Author URI: http://incrediblebytes.com
 * Description: Adds ability to add custom sidebars (a theme has to support it).
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Version: 1.4.0
 *
 * @package Dm3Sidebars
 */

/*
Copyright (C) 2015 http://incrediblebytes.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

function dm3sb_map_id( $id ) {
	if ( is_numeric( $id ) ) {
		$id = '';
	} else {
		$id = sanitize_key( $id );
	}

	return $id;
}

if ( is_admin() ) {
	/**
	 * Load text domain.
	 */
	function dm3sb_textdomain() {
		load_plugin_textdomain( 'dm3-sidebars', false, 'dm3-sidebars/languages' );
	}
	add_action( 'plugins_loaded', 'dm3sb_textdomain' );

	/**
	 * Add admin menu.
	 */
	function dm3sb_add_theme_menu() {
		add_theme_page(
			__( 'Custom Sidebars', 'dm3-sidebars' ),
			__( 'Custom Sidebars', 'dm3-sidebars' ),
			'manage_options',
			'dm3sb_options_page',
			'dm3sb_options_page'
		);
	}
	add_action( 'admin_menu', 'dm3sb_add_theme_menu' );

	/**
	 * Render options page.
	 */
	function dm3sb_options_page() {
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		$post_types = dm3sb_get_post_types();
		$sidebars = get_option( 'dm3sb_sidebars', array() );

		require_once 'options-view.php';
	}

	/**
	 * Process ajax calls.
	 */
	function dm3sb_ajax() {
		if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'dm3sb_edit_sidebar' ) ) {
			return;
		}

		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		$action = isset( $_POST['action'] ) ? $_POST['action'] : '';
		$name = isset( $_POST['name'] ) ? trim( $_POST['name'] ) : '';
		$response = array( 'status' => 'error' );

		if ( ! preg_match( '/^[a-z0-9\s]+$/i', $name ) ) {
			$name = '';
		}

		if ( ! empty( $name ) ) {
			// Get custom sidebars.
			$sidebars = get_option( 'dm3sb_sidebars', array() );

			switch ( $action ) {
				// Add sidebar.
				case 'dm3sb_add_sidebar':
					$sidebar_id = sanitize_key( $name );

					if ( ! in_array( $name, $sidebars ) && ! isset( $sidebars[ $sidebar_id ] ) ) {
						// Sidebar does not exist.
						$sidebars[ $sidebar_id ] = $name;

						if ( update_option( 'dm3sb_sidebars', $sidebars ) ) {
							$response['status'] = 'success';
						}
					} else {
						$response['message'] = __( 'Sidebar already exists', 'dm3-sidebars' );
					}
					break;

				// Delete sidebar.
				case 'dm3sb_delete_sidebar':
					$array_key = array_search( $name, $sidebars );

					if ( $array_key !== false ) {
						unset( $sidebars[ $array_key ] );

						if ( update_option( 'dm3sb_sidebars', $sidebars ) ) {
							$response['status'] = 'success';
						}
					}
					break;

				// Update sidebar.
				case 'dm3sb_update_sidebar':
					$id = isset( $_POST['id'] ) ? dm3sb_map_id( $_POST['id'] ) : '';

					if ( ! empty( $name ) && ! empty( $id ) ) {
						if ( ! isset( $sidebars[ $id ] ) ) {
							$new_sidebars = array();

							foreach ( $sidebars as $sid => $sname ) {
								if ( $sname == $name ) {
									$new_sidebars[ $id ] = $sname;
								} else {
									$new_sidebars[ $sid ] = $sname;
								}
							}

							update_option( 'dm3sb_sidebars', $new_sidebars );

							$response['status'] = 'success';
							$response['message'] = sprintf( __( 'Sidebar "%s" has been updated', 'dm3-sidebars' ), esc_html( $name ) );
						} else {
							$response['message'] = __( 'Please enter a unique sidebar id', 'dm3-sidebars' );
						}
					} else {
						$response['message'] = __( 'Please enter sidebar id', 'dm3-sidebars' );
					}
					break;
			}
		} else {
			$response['message'] = __( 'Sidebar name contains invalid characters', 'dm3-sidebars' );
		}

		echo json_encode( $response );
		exit();
	}

	/**
	 * Get supported post types.
	 */
	function dm3sb_get_post_types() {
		return apply_filters( 'dm3sb_post_types', array( 'post', 'page' ) );
	}

	/**
	 * Add meta box.
	 */
	function dm3sb_add_meta_box() {
		$post_types = dm3sb_get_post_types();

		foreach ( $post_types as $post_type ) {
			add_meta_box(
				'dm3sb_meta_box',
				__('Custom sidebar', 'dm3-options'),
				'dm3sb_show_meta_box',
				$post_type,
				'normal',
				'high'
			);
		}
	}

	/**
	 * Show meta box.
	 */
	function dm3sb_show_meta_box( $post ) {
		// Get custom sidebars.
		$sidebars = get_option( 'dm3sb_sidebars', array() );

		// Get current sidebar.
		$current_sidebar = get_post_meta( $post->ID, 'dm3sb_custom_sidebar', true );

		// Show form.
		require_once 'meta-box-view.php';
	}

	/**
	 * Save meta box options.
	 */
	function dm3sb_save_meta_box( $post_id ) {
		if ( ! isset( $_POST['dm3sb_meta_box_nonce'] ) || ! wp_verify_nonce( $_POST['dm3sb_meta_box_nonce'], 'dm3sb_meta_box' ) ) {
			return $post_id;
		}

		$post_type = isset( $_POST['post_type'] ) ? $_POST['post_type'] : '';

		if ( empty( $post_type ) ) {
			return $post_id;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		if ( 'page' == $post_type ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			}
		} else {
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}
		}

		$custom_sidebar = sanitize_text_field( $_POST['dm3sb_custom_sidebar'] );

		update_post_meta( $post_id, 'dm3sb_custom_sidebar', $custom_sidebar );
	}

	add_action( 'add_meta_boxes', 'dm3sb_add_meta_box' );
	add_action( 'save_post', 'dm3sb_save_meta_box' );
	add_action( 'wp_ajax_dm3sb_add_sidebar', 'dm3sb_ajax' );
	add_action( 'wp_ajax_dm3sb_delete_sidebar', 'dm3sb_ajax' );
	add_action( 'wp_ajax_dm3sb_update_sidebar', 'dm3sb_ajax' );
}

/**
 * Register sidebars.
 */
function dm3sb_register_sidebars() {
	$sidebars = get_option( 'dm3sb_sidebars', array() );
	$i = 1;

	foreach ( $sidebars as $id => $sidebar ) {
		$args = array(
			'name'          => $sidebar,
			'description'   => __( 'Dm3Sidebars plugin custom sidebar', 'dm3-sidebars' ),
			'before_widget' => '<div class="widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		);

		$id_attr = dm3sb_map_id( $id );

		if ( $id_attr ) {
			$args['id'] = $id_attr;
		}

		$args = apply_filters( 'dm3sb_sidebar_args', $args );

		register_sidebar( $args );

		++$i;
	}
}
add_action( 'widgets_init', 'dm3sb_register_sidebars', 100 );

/**
 * Show sidebar.
 *
 * @param string $default_sidebar
 */
function dm3sb_dynamic_sidebar( $default_sidebar ) {
	global $post;

	if ( $post ) {
		$custom_sidebar = get_post_meta( $post->ID, 'dm3sb_custom_sidebar', true );

		if ( $custom_sidebar ) {
			dynamic_sidebar( $custom_sidebar );

			return;
		}
	}

	dynamic_sidebar( $default_sidebar );
}
